package ssh_ultimate

import (
	"github.com/kevinburke/ssh_config"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/go/tests"
	"testing"
)

func proxyJumpsSSHConfig() string {
	return `
Host *
  ProxyJump example.com, example2.com
`
}

func TestParseProxyJumps(t *testing.T) {
	Prepare(t, proxyJumpsSSHConfig, func(t *testing.T, sshConfig *ssh_config.Config) {
		t.Run("without ssh config", func(t *testing.T) {
			proxies, err := parseProxyJumps(nil, "foobar")
			tests.AssertNoError(t, err)
			assert.Equal(t, 0, len(proxies))
		})

		t.Run("with ssh config", func(t *testing.T) {
			proxies, err := parseProxyJumps(sshConfig, "foobar")
			tests.AssertNoError(t, err)
			assert.Equal(t, 2, len(proxies))

			assert.Equal(t, "example.com", proxies[0].Host)
			assert.Equal(t, "example2.com", proxies[1].Host)
		})
	})
}
