package ssh_ultimate

import "fmt"

type SSHUltimateError struct {
	format string
	args   []any
}

func (e *SSHUltimateError) Error() string {
	return fmt.Sprintf(e.format, e.args...)
}

func errorf(format string, args ...any) *SSHUltimateError {
	return &SSHUltimateError{
		format: format,
		args:   args,
	}
}
