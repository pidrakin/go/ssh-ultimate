package ssh_ultimate

import (
	"fmt"
	"github.com/kevinburke/ssh_config"
	"github.com/mitchellh/go-homedir"
	"gitlab.com/pidrakin/go/interactive"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
	"io"
	"net"
	"os"
	"strings"
)

func parseIdentityFile(sshConfig *ssh_config.Config, host string, identityFile string) (string, error) {
	var err error
	if identityFile != "" {
		return identityFile, err
	}

	if sshConfig != nil {
		identityFile, err = sshConfig.Get(host, "IdentityFile")
		if err != nil {
			return "", err
		}
		if identityFile != "" {
			return identityFile, err
		}
	}

	return "", err
}

func passwordAuthMethod(reader io.Reader, writer io.Writer, id string) ssh.AuthMethod {
	return ssh.PasswordCallback(func() (string, error) {
		response := os.Getenv("SSHPASS")
		if response == "" {
			_, err := fmt.Fprintf(writer, "[%s] Password required: ", id)
			if err != nil {
				return "", err
			}
			response, err = interactive.PromptLine(reader, true)
			if err != nil {
				return "", err
			}
			response = strings.TrimSuffix(response, "\n")
			_, err = fmt.Fprintf(writer, "\n")
			if err != nil {
				return "", err
			}
		}
		return response, nil
	})
}

var agentClient agent.Agent

func sshAgentAuthMethod() (ssh.AuthMethod, error) {
	if agentClient == nil {
		socket := os.Getenv("SSH_AUTH_SOCK")
		conn, err := net.Dial("unix", socket)
		if err != nil {
			return nil, errorf("failed to open SSH_AUTH_SOCK: %v", err)
		}

		agentClient = agent.NewClient(conn)
	}
	return ssh.PublicKeysCallback(agentClient.Signers), nil
}

func privateKeyAuthMethod(file string, keyPass string) (ssh.AuthMethod, error) {
	var key ssh.Signer
	var b []byte
	normalizedPath, err := homedir.Expand(file)
	if err != nil {
		return nil, err
	}
	b, err = os.ReadFile(normalizedPath)
	if err != nil {
		return nil, err
	}
	if keyPass == "" {
		key, err = ssh.ParsePrivateKey(b)
	} else {
		key, err = ssh.ParsePrivateKeyWithPassphrase(b, []byte(keyPass))
	}

	if err != nil {
		if _, ok := err.(*ssh.PassphraseMissingError); ok {
			logger.Errorf("ssh identity file needs passphrase")
		}
		return nil, err
	}
	return ssh.PublicKeys(key), nil
}

func parseAuthMethods(reader io.Reader, writer io.Writer, sshConfig *ssh_config.Config, host string, identityFilePath string, identityKeyPass string, id string) ([]ssh.AuthMethod, error) {
	var authMethods []ssh.AuthMethod

	var err error

	identityFilePath, err = parseIdentityFile(sshConfig, host, identityFilePath)
	if err != nil {
		return nil, err
	}

	if identityFilePath != "" {
		authMethod, privErr := privateKeyAuthMethod(identityFilePath, identityKeyPass)
		if privErr != nil {
			return nil, privErr
		}
		authMethods = append(authMethods, authMethod)
	}

	// TODO: not yet used; don't know how
	//var identitiesOnly string
	//if sshConfig != nil {
	//	identitiesOnly, err = sshConfig.Get(host, "IdentitiesOnly")
	//	if err != nil {
	//		return nil, err
	//	}
	//}

	authMethod, agentErr := sshAgentAuthMethod()
	if agentErr == nil {
		authMethods = append(authMethods, authMethod)
	}

	var passwordAuthentication string
	if sshConfig != nil {
		passwordAuthentication, err = sshConfig.Get(host, "PasswordAuthentication")
		if err != nil {
			return nil, err
		}
	}

	if passwordAuthentication != "no" {
		authMethods = append(authMethods, passwordAuthMethod(reader, writer, id))
	}

	return authMethods, nil
}
