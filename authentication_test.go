package ssh_ultimate

import (
	"bytes"
	"github.com/kevinburke/ssh_config"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/go/tests"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
	"strings"
	"testing"
)

func authenticationSSHConfig() string {
	return `
Host SSHAgent
  PasswordAuthentication no

Host SSHAgentIdentity
  PasswordAuthentication no
  IdentityFile ~/.ssh/id_ed25519
`
}

func Test_parseAuthMethods(t *testing.T) {
	Prepare(t, authenticationSSHConfig, func(t *testing.T, sshConfig *ssh_config.Config) {
		privateKey, err := ssh.ParseRawPrivateKey(IdentityFile)
		tests.AssertNoError(t, err)
		key := agent.AddedKey{
			PrivateKey: privateKey,
		}
		agentClient = agent.NewKeyring()
		defer func() {
			agentClient = nil
		}()
		err = agentClient.Add(key)
		tests.AssertNoError(t, err)

		t.Run("ssh agent", func(t *testing.T) {
			reader := strings.NewReader("")
			var buf bytes.Buffer
			authMethods, err := parseAuthMethods(reader, &buf, sshConfig, "SSHAgent", "", "", "")
			tests.AssertNoError(t, err)
			assert.NotNil(t, authMethods)
			assert.Equal(t, 1, len(authMethods))
		})
		t.Run("ssh agent + pw", func(t *testing.T) {
			reader := strings.NewReader("")
			var buf bytes.Buffer
			authMethods, err := parseAuthMethods(reader, &buf, sshConfig, "unknown", "", "", "")
			tests.AssertNoError(t, err)
			assert.NotNil(t, authMethods)
			assert.Equal(t, 2, len(authMethods))
		})
		t.Run("ssh agent + identity", func(t *testing.T) {
			reader := strings.NewReader("")
			var buf bytes.Buffer
			authMethods, err := parseAuthMethods(reader, &buf, sshConfig, "SSHAgentIdentity", "", "", "")
			tests.AssertNoError(t, err)
			assert.NotNil(t, authMethods)
			assert.Equal(t, 2, len(authMethods))
		})
		t.Run("ssh agent + identity cli", func(t *testing.T) {
			reader := strings.NewReader("")
			var buf bytes.Buffer
			authMethods, err := parseAuthMethods(reader, &buf, sshConfig, "SSHAgent", "~/.ssh/id_ed25519", "", "")
			tests.AssertNoError(t, err)
			assert.NotNil(t, authMethods)
			assert.Equal(t, 2, len(authMethods))
		})
	})
}
