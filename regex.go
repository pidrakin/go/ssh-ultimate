package ssh_ultimate

import "regexp"

var UserHostPortRegex = regexp.MustCompile(`^(?:ssh://)?(?:(?P<user>[^@:\s/]+)@)?(?P<host>[^:\s/]+)(?::(?P<port>\d+))?$`)
