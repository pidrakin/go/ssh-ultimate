package ssh_ultimate

import (
	"gitlab.com/pidrakin/go/regex"
)

type proxyJump struct {
	User string
	Host string
	Port string
}

func newProxyJump(proxyJumpConfig string) *proxyJump {
	proxy := &proxyJump{}
	groups := regex.MatchGroups(UserHostPortRegex, proxyJumpConfig)
	if h, ok := groups["host"]; ok {
		proxy.Host = h
	}
	if p, ok := groups["port"]; ok {
		proxy.Port = p
	}
	if u, ok := groups["user"]; ok {
		proxy.User = u
	}
	return proxy
}
