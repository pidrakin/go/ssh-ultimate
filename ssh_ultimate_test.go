package ssh_ultimate

import (
	"bytes"
	"fmt"
	slog "github.com/go-eden/slf4go"
)

var (
	driver slog.Driver
)

type TestDriver struct {
	Buf bytes.Buffer
}

func (ts *TestDriver) Name() string {
	return "test"
}

func (ts *TestDriver) Print(l *slog.Log) {
	var msg string
	if l.Format != nil {
		msg = fmt.Sprintf(*l.Format, l.Args...)
	} else {
		msg = fmt.Sprint(l.Args...)
	}
	ts.Buf.WriteString(msg)
}

func (ts *TestDriver) GetLevel(logger string) slog.Level {
	return slog.TraceLevel
}

func init() {

}
