package ssh_ultimate

import "github.com/kevinburke/ssh_config"

func parsePort(sshConfig *ssh_config.Config, host string, defaultPort string) (string, error) {
	if defaultPort != "" {
		return defaultPort, nil
	}

	if sshConfig != nil {
		port, err := sshConfig.Get(host, "Port")
		if err != nil {
			return "", err
		}
		if port != "" {
			return port, nil
		}
	}

	return "22", nil
}
