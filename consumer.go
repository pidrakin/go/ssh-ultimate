package ssh_ultimate

import (
	"bufio"
	"bytes"
	"fmt"
	"github.com/dlsniper/debugger"
	"gitlab.com/pidrakin/go/interactive"
	"io"
	"sync"
)

type remote struct {
	wg          *sync.WaitGroup
	interactive bool
	remoteOut   io.Reader
	remoteIn    io.Writer
}

func (r *remote) consume(remoteType RemoteType, localIn io.Reader, localOut io.Writer) {
	defer r.wg.Done()

	debugger.SetLabels(func() []string {
		return []string{
			"routine", "handleRemoteCommunication",
			"type", string(remoteType),
		}
	})

	reader := bufio.NewReader(r.remoteOut)
	var line []byte
	var response []byte

	for {
		b, err := reader.ReadByte()
		if err != nil {
			if err == io.EOF {
				return
			}
			_ = fmt.Errorf("%#v", err)
		}
		if b == '\r' {
			continue
		}
		line = append(line, b)
		if b == '\n' {
			if len(response) > 0 && bytes.Equal(bytes.TrimSpace(line), bytes.TrimSpace(response)) {
				response = []byte{}
			} else {
				r.print(localOut, line)
			}
			line = []byte{}
			continue
		}
		if r.interactive && bytes.HasSuffix(bytes.TrimSpace(line), []byte{':'}) && reader.Buffered() == 0 {
			r.print(localOut, line)
			response = r.prompt(localIn, line)
			line = []byte{}
			r.send(response)
		}
	}
}

func (r *remote) send(message []byte) {
	_, err := fmt.Fprintf(r.remoteIn, string(message))
	if err != nil {
		_ = fmt.Errorf("%#v", err)
	}
}

func (r *remote) print(localOut io.Writer, line []byte) {
	_, err := fmt.Fprintf(localOut, string(line))
	if err != nil {
		_ = fmt.Errorf("%#v", err)
	}
}

func (r *remote) prompt(localIn io.Reader, line []byte) []byte {
	isPwPrompt := bytes.Contains(line, []byte("[sudo] password for"))
	response, err := interactive.PromptLine(localIn, isPwPrompt)
	if err != nil {
		_ = fmt.Errorf("%#v", err)
	}
	return []byte(response)
}

type local struct {
	wg          *sync.WaitGroup
	localIn     io.Reader
	localStdout io.Writer
	localStderr io.Writer
}

type RemoteType string

var (
	STDOUT RemoteType = "stdout"
	STDERR RemoteType = "stderr"
)

type Consumer struct {
	wg        *sync.WaitGroup
	wgCounter int
	remotes   map[RemoteType]*remote
	local     *local
}

func NewConsumer() *Consumer {
	consumer := &Consumer{
		wg:      &sync.WaitGroup{},
		remotes: map[RemoteType]*remote{},
	}
	return consumer
}

func (c *Consumer) AddRemote(remoteType RemoteType, interactive bool, remoteOut io.Reader, remoteIn io.Writer) {
	c.wgCounter++

	remote := &remote{
		wg:          c.wg,
		interactive: interactive,
		remoteOut:   remoteOut,
		remoteIn:    remoteIn,
	}
	c.remotes[remoteType] = remote
}

func (c *Consumer) AddLocal(localIn io.Reader, localStdout io.Writer, localStderr io.Writer) {
	local := &local{
		wg:          c.wg,
		localIn:     localIn,
		localStdout: localStdout,
		localStderr: localStderr,
	}
	c.local = local
}

func (c *Consumer) Consume() {
	if len(c.remotes) < 1 {
		logger.Errorf("at least one remote needs to be available")
		return
	}
	if c.local == nil {
		logger.Errorf("a local has to be specified")
	}

	c.wg.Add(c.wgCounter)

	for t, r := range c.remotes {
		var localOut io.Writer
		if t == STDOUT {
			localOut = c.local.localStdout
		} else {
			localOut = c.local.localStderr
		}
		go r.consume(t, c.local.localIn, localOut)
	}
}

func (c *Consumer) Wait() {
	c.wg.Wait()
}
