package ssh_ultimate

import (
	"fmt"
	"github.com/kevinburke/ssh_config"
	"gitlab.com/pidrakin/go/containers"
	"gitlab.com/pidrakin/go/maps"
	"gitlab.com/pidrakin/go/slices"
	"golang.org/x/crypto/ssh"
	"io"
	"os"
	"path/filepath"
)

func systemConfigFinder() string {
	return filepath.Join("/", "etc", "ssh", "ssh_config")
}

func userConfigFinder() string {
	return filepath.Join(os.Getenv("HOME"), ".ssh", "config")
}

type Config struct {
	Name             string
	HostName         string
	Port             string
	proxyJumpConfigs []*proxyJump
	ProxyJumps       []string
	ClientConfig     *ssh.ClientConfig
}

func loadConfig() (*ssh_config.Config, error) {
	file, err := os.Open(userConfigFinder())
	if err != nil {
		return nil, err
	}
	cfg, err := ssh_config.Decode(file)
	if err != nil {
		file, err = os.Open(systemConfigFinder())
		if err != nil {
			return nil, err
		}
		cfg, err = ssh_config.Decode(file)
	}
	return cfg, err
}

func NewConfigs(host string, options ...func(*Parameterizable) error) ([]*Config, error) {
	p, err := NewParameterizable(options...)
	if err != nil {
		return nil, err
	}

	reader := p.Reader
	writer := p.Writer
	port := p.Port
	user := p.User
	identityFilePath := p.IdentityFilePath
	identityFilePassphrase := p.IdentityFilePassphrase

	configMap := map[string]*Config{}

	sshConfig, err := loadConfig()
	if err != nil {
		_, ok := err.(*os.PathError)
		if !ok {
			return nil, err
		}
	}

	proxies := containers.New[*proxyJump]()
	var orderedList []string
	var cfg *Config
	for ok := true; ok; ok = !proxies.Empty() {
		var proxy *proxyJump = nil
		if cfg != nil {
			proxy, err = proxies.Pop()
			host = proxy.Host
			port = proxy.Port
			user = proxy.User
		}
		cfg, err = newConfig(reader, writer, sshConfig, host, port, user, identityFilePath, identityFilePassphrase)
		if err != nil {
			return nil, err
		}

		// reset parameters from function
		host = ""
		port = ""
		user = ""
		identityFilePath = ""
		identityFilePassphrase = ""

		configMap[cfg.Name] = cfg
		orderedList = append(orderedList, cfg.Name)

		for i := len(cfg.proxyJumpConfigs) - 1; i >= 0; i-- {
			proxies.Push(cfg.proxyJumpConfigs[i])
		}
	}

	for _, cfg := range maps.Values(configMap) {
		for _, proxy := range cfg.proxyJumpConfigs {
			if cfg2, ok := configMap[proxy.Host]; ok {
				cfg.ProxyJumps = append(cfg.ProxyJumps, cfg2.Name)
			}
		}
	}

	configs := slices.Map(slices.Reverse(orderedList), func(n string) *Config {
		return configMap[n]
	})

	return configs, nil
}

func newConfig(reader io.Reader, writer io.Writer, sshConfig *ssh_config.Config, host string, port string, user string, identityFilePath string, identityKeyPass string) (*Config, error) {
	var err error
	cfg := &Config{}
	cfg.Name = host
	cfg.HostName, err = parseHostName(sshConfig, host)
	if err != nil {
		return nil, err
	}

	cfg.Port, err = parsePort(sshConfig, host, port)
	if err != nil {
		return nil, err
	}

	user, err = parseUser(sshConfig, host, user)
	if err != nil {
		return nil, err
	}

	cfg.proxyJumpConfigs, err = parseProxyJumps(sshConfig, host)
	if err != nil {
		return nil, err
	}

	authMethods, err := parseAuthMethods(reader, writer, sshConfig, host, identityFilePath, identityKeyPass, fmt.Sprintf("%s@%s:%s", user, cfg.HostName, cfg.Port))
	if err != nil {
		return nil, err
	}

	cfg.ClientConfig = &ssh.ClientConfig{
		User:            user,
		Auth:            authMethods,
		HostKeyCallback: TrustedHostKeyCallback(""),
	}

	//var strictHostKeyChecking string
	//if sshConfig != nil {
	//	strictHostKeyChecking, err = sshConfig.Get(host, "StrictHostKeyChecking")
	//	if err != nil {
	//		return nil, err
	//	}
	//}
	//if strictHostKeyChecking == "no" {
	//	cfg.ClientConfig.HostKeyCallback = ssh.InsecureIgnoreHostKey()
	//}

	return cfg, nil
}
