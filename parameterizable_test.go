package ssh_ultimate

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/go/tests"
	"testing"
)

type TestType struct {
	Parameterizable
}

func TestNewParameterizable(t *testing.T) {
	p, err := NewParameterizable(PortOption("22"))
	tests.AssertNoError(t, err)
	tt := &TestType{
		Parameterizable: *p,
	}
	assert.Equal(t, "22", tt.Port)
}

func TestNewParameterizableOverride(t *testing.T) {
	p, err := NewParameterizable(
		PortOption("22"),
		PortOption("23"),
	)
	tests.AssertNoError(t, err)
	tt := &TestType{
		Parameterizable: *p,
	}
	assert.Equal(t, "23", tt.Port)
}
