package ssh_ultimate

import (
	_ "embed"
	slog "github.com/go-eden/slf4go"
)

var (
	logger slog.Logger
	//go:embed keys/id_ed25519
	IdentityFile []byte
	//go:embed keys/p_id_ed25519
	ProtectedIdentityFile       []byte
	ProtectedIdentityPassphrase string = "test"
)

func init() {
	logger = slog.NewLogger("SSH Ultimate")
}
