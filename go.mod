module gitlab.com/pidrakin/go/ssh-ultimate

go 1.19

require (
	github.com/dlsniper/debugger v0.6.0
	github.com/go-eden/slf4go v1.1.2
	github.com/kevinburke/ssh_config v1.2.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/pkg/sftp v1.13.5
	github.com/stretchr/testify v1.8.1
	gitlab.com/pidrakin/go/containers v0.0.1
	gitlab.com/pidrakin/go/interactive v0.0.1
	gitlab.com/pidrakin/go/maps v0.0.3
	gitlab.com/pidrakin/go/regex v0.0.1
	gitlab.com/pidrakin/go/slices v0.0.2
	gitlab.com/pidrakin/go/tests v0.0.1
	golang.org/x/crypto v0.3.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-eden/common v0.1.14 // indirect
	github.com/go-eden/routine v1.0.1 // indirect
	github.com/kr/fs v0.1.0 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gitlab.com/pidrakin/go/output v0.0.2 // indirect
	golang.org/x/exp v0.0.0-20221114191408-850992195362 // indirect
	golang.org/x/sys v0.2.0 // indirect
	golang.org/x/term v0.2.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
