package ssh_ultimate

import (
	"golang.org/x/crypto/ssh"
	"strconv"
)

func Dial(host string, port int, user string, identityFilePath string, identityFilePassphrase string) (*ssh.Client, error) {
	cfgs, err := NewConfigs(
		host,
		PortOption(strconv.Itoa(port)),
		UserOption(user),
		IdentityFilePathOption(identityFilePath),
		IdentityFilePassphraseOption(identityFilePassphrase),
	)
	if err != nil {
		return nil, err
	}
	return dial(cfgs)
}

func dial(configs []*Config) (*ssh.Client, error) {
	var err error
	var client *ssh.Client
	for _, cfg := range configs {
		if client == nil {
			client, err = ssh.Dial("tcp", cfg.HostName+":"+cfg.Port, cfg.ClientConfig)
			if err != nil {
				return nil, err
			}
		} else {
			conn, err := client.Dial("tcp", cfg.HostName+":"+cfg.Port)
			if err != nil {
				return nil, err
			}

			ncc, chans, reqs, err := ssh.NewClientConn(conn, cfg.HostName+":"+cfg.Port, cfg.ClientConfig)
			if err != nil {
				return nil, err
			}

			client = ssh.NewClient(ncc, chans, reqs)
		}
	}
	return client, nil
}
